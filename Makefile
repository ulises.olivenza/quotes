start-dependencies: 
	docker compose up -d redis
	
stop-dependencies: 
	docker compose redis stop

clean-dependencies: 
	docker compose redis stopdown 

test: start-dependencies
	./scripts/test.sh
run: 
	docker-compose up --build -d
	docker-compose logs -f quotes

stop: 
	docker-compose stop

