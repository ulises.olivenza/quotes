# Quotes

## Basics
### Prerequisites
docker at least 20.10.6

### Development
Just hit 
```bash
make start-dependencies
```

Install golang !!

And you change the code freely.

### Test 
Just hit
```bash
make test
```
This should trigger a go docker image and redis to execute the tests

### Run
Just hit
```bash
make run
````

This should trigger a go docker with the quotes app and redis

Default host is 
```bash
http://localhost:8080
```

## Design

### Tests

#### Unit Tests
There are tests for each package.  
But, there's differences regarding dependencies:

1. If there are not external dependencies outside the package: dependencies are real code, no mocks. That makes it both easy and more accurate to test due to the simplicity of the packages. In some case we need redis to be up since it's our "database"

2. If the package depends on another package: Like middleware and controllers packages, the dependencies are mocked.

#### Integration Tests
There's no integration test.
I usually consider integration test a separate project, outside the app. That and time constrains are the reason there's none

### Monitoring and Logging
I just add a Logging Middleware which is login every request to the application. For production use we might want to add some logs here and there, since this is a simple exercise I prefer to keep it simple

There's not integration with any monitoring, for production use we will want to add statds or any other integration

There's no healthcheck, Readiness or any other endpoint regarding deployment since the problem statement does not mention anything about it.

### HA
The way the app is done, it should be ready fot HA deployment. That is, more than one instance. All the needed states is stored at redis.

### Tokens
For the token we are using an jwt token signed with HS512.  
See an example here
```
https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ0aW1lcyI6NSwiZXhwIjoxNjIxODgwMjgwMDk5NDQ3MDAwLCJzdWIiOiJ1c2VybmFtZSJ9.2oBztAiK_DwmMF5-xc-gMdzRwnvXrVxxdm1cyH-7QkRvop3kWzmcbdG971Goc7c2QUFemJzLc-pwmVoxCc1Ikg
```

The payload is as follows:
```json
{
  "times": 5,
  "exp": 1621880280099447000,
  "sub": "username"
}
```

The sub property indicates the user and the exp indicates, in nano epoch, the expiration date. By default 1 min.

So, the expiration time is handle by the jwt standard

Regardy the 5 times limit. It's implementing in redis as follows:

1. Hash the token, so we don't store the token in redis for security reasons, with a kdf pbkdf2 algorithm.
2. Get the user name from the jwt token once the expiration has been verfied.
3. Store 5 integers in a List in Redis.T he key begin the hash of the token and the user.

So, when the token is used

1. Hash the token and concatenate the user to recreate the redis key
2. Consume one element of the List
    2.1 if it is not empty remove on element
        2.1.1 if the list is emptied after the element is removed the entry is deleted in redis. 
        That means that if we search again the key, it will be missing. Therefore rejecting the access.

Redis token keys are garbage collected by redis using the redis EXP command, which expires the entry after a given time. In our case one minute and 5 seconds. That 5 seconds are just in case the clocks are a bit off and to make sure it's deleted after the jwt token is expired.
That does not affect the token expirated, since the jtw token itself contains the timestamp to validate token expiration.


### Quotes
Quotes are parsed and stored in redis as a map under "quotes" key


### Shared links
Shared links are generated and stored in redis as a list with only one element: the quote content. Key is the "share_url" part of 
```
/share/$share_url
```

That, efectivily copies the content when the link is created. Preserving as it was when the link was generated.

They follow the same idea as the tokens: when it's acceded, the list will be emptied. Redis will delete the entry since it contains an empty list. Therefore, not allowing it to be acceced again.
