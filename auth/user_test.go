package auth_test

import (
	"testing"
	"tgracchus/quotes/auth"
)

func TestAuthService(t *testing.T) {
	user, err := basicAuthService.Auth("username", "password")
	if err != nil {
		t.Fatal(err)
	}
	if user.Username != "username" {
		t.Fatal("Expected username")
	}
}

func TestAuthServiceUserNotFound(t *testing.T) {
	_, err := basicAuthService.Auth("anotherusername", "password")
	if err == nil {
		t.Fatal("Expected Error")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}
func TestAuthServicePasswordDoesNotMatch(t *testing.T) {
	_, err := basicAuthService.Auth("username", "anotherpassword")
	if err == nil {
		t.Fatal("Expected Error")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}
