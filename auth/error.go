package auth

type UnauthorizeError struct {
	Err error
}

func (u *UnauthorizeError) Error() string {
	return u.Err.Error()
}
