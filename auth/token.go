package auth

import (
	"context"
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Generates a token from a given token context.
type TokenGenerator interface {
	Generate(ctx context.Context, tokenGenInput *TokenGenInput) (*TokenGenOutput, error)
}

// Parse and validate a token from a string.
type TokenValidator interface {
	Validate(ctx context.Context, token string) (*TokenValOutput, error)
}

// Generates a token from a given token context and a secret.
func NewJWTSecretTokenGenerator(userFinder UserFinder) TokenGenerator {
	return &jwtSecretTokenGen{userFinder: userFinder}
}
func NewJWTSecretTokenValidator(userFinder UserFinder) TokenValidator {
	return &jwtSecretTokenVal{userFinder: userFinder}
}

// All the params needed to generate a token
type TokenGenInput struct {
	Username  string
	ExpiresAt time.Time
	Times     int
}

// All the params used to generate a token
type TokenGenOutput struct {
	Token       string
	TokenSecret []byte
}

// All the params after validation a token
type TokenValOutput struct {
	ExpiresAt   time.Time
	Times       int
	Username    string
	TokenSecret []byte
}

type jwtSecretTokenGen struct {
	userFinder UserFinder
}

type jwtSecretTokenVal struct {
	userFinder UserFinder
}

type JwtTokenInfo struct {
	Times int `json:"times"`
	jwt.StandardClaims
}

func (c JwtTokenInfo) Valid() error {
	now := time.Now().UnixNano()
	valid := c.VerifyExpiresAt(now, true)
	if !valid {
		return &UnauthorizeError{errors.New("token is expired")}
	}
	if c.Times != 5 {
		return &UnauthorizeError{errors.New("invalid times claims")}
	}
	return nil
}

func (g *jwtSecretTokenGen) Generate(ctx context.Context, tokenGenInput *TokenGenInput) (*TokenGenOutput, error) {
	// Create the Claims
	claims := JwtTokenInfo{
		5,
		jwt.StandardClaims{
			ExpiresAt: tokenGenInput.ExpiresAt.UnixNano(),
			Subject:   tokenGenInput.Username,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	user, err := g.userFinder.Find(tokenGenInput.Username)
	if err != nil {
		return nil, err
	}
	signedToken, err := token.SignedString(user.TokenSecret)
	if err != nil {
		return nil, err
	}
	return &TokenGenOutput{
		Token:       signedToken,
		TokenSecret: user.TokenSecret,
	}, nil
}

func (v *jwtSecretTokenVal) Validate(ctx context.Context, tokenString string) (*TokenValOutput, error) {
	var user *User
	token, err := jwt.ParseWithClaims(tokenString, &JwtTokenInfo{}, func(token *jwt.Token) (interface{}, error) {
		if claims, ok := token.Claims.(*JwtTokenInfo); ok {
			var err error
			user, err = v.userFinder.Find(claims.Subject)
			if err != nil {
				return "", err
			}
			return user.TokenSecret, nil
		} else {
			return nil, &UnauthorizeError{errors.New("invalid claims format")}
		}
	})
	if err != nil {
		if err, ok := err.(*jwt.ValidationError); ok {
			return nil, &UnauthorizeError{err}
		}
		return nil, err
	}
	if claims := token.Claims.(*JwtTokenInfo); token.Valid {
		tokenContext := &TokenValOutput{
			ExpiresAt:   time.Unix(0, claims.ExpiresAt),
			Times:       claims.Times,
			Username:    user.Username,
			TokenSecret: user.TokenSecret,
		}
		return tokenContext, nil
	} else {
		return nil, err
	}
}
