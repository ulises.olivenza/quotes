package auth

import (
	"context"
	"crypto/sha512"
	"errors"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"golang.org/x/crypto/pbkdf2"
)

// From a given token it creates a key
type SaltedHasher func(token string, salt []byte) (string, error)

// Create a Pbkdf2 hasher
func NewPbkdf2Hasher() SaltedHasher {
	return func(token string, salt []byte) (string, error) {
		derivedKey := pbkdf2.Key([]byte(token), salt, 512, 128, sha512.New)
		return fmt.Sprintf("%x", derivedKey), nil
	}
}

// Creates a new redis token generator
func NewRedisTokenGenerator(
	rdb *redis.Client,
	tokenHasher SaltedHasher,
	tokenGenerator TokenGenerator,
	redisKeyGenerator RedisKeyGenerator,
) TokenGenerator {
	return &redisTokenGenerator{rdb: rdb, hasher: tokenHasher, tokenGenerator: tokenGenerator, keyGenerator: redisKeyGenerator}
}

type redisTokenGenerator struct {
	rdb            *redis.Client
	hasher         SaltedHasher
	tokenGenerator TokenGenerator
	keyGenerator   RedisKeyGenerator
}

func (g *redisTokenGenerator) Generate(ctx context.Context, tokenGenInput *TokenGenInput) (*TokenGenOutput, error) {
	tokenGenOutput, err := g.tokenGenerator.Generate(ctx, tokenGenInput)
	if err != nil {
		return nil, err
	}
	tokenHash, err := g.hasher(tokenGenOutput.Token, tokenGenOutput.TokenSecret)
	if err != nil {
		return nil, err
	}
	key := g.keyGenerator(tokenGenInput.Username, tokenHash)
	values := make([]interface{}, tokenGenInput.Times)
	for i := 0; i < tokenGenInput.Times; i++ {
		values[i] = 1
	}
	_, err = g.rdb.LPush(ctx, key, values...).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			return nil, &UnauthorizeError{err}
		}
		return nil, err
	}
	//Adding five seconds just for the processint time of the token generation
	_, err = g.rdb.ExpireAt(ctx, key, tokenGenInput.ExpiresAt.Add(time.Second*5)).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			return nil, &UnauthorizeError{err}
		}
		return nil, err
	}
	return tokenGenOutput, nil
}

type RedisKeyGenerator func(username string, token string) string

func NewRedisKeyGenerator() RedisKeyGenerator {
	return func(username string, tokenHash string) string {
		return username + "/" + tokenHash
	}
}

// Creates a new redis token generator
func NewRedisTokenValidator(
	rdb *redis.Client,
	tokenHasher SaltedHasher,
	tokenValidator TokenValidator,
	redisKeyGenerator RedisKeyGenerator,
) TokenValidator {
	return &redisTokenValidator{rdb: rdb, tokenHasher: tokenHasher, tokenValidator: tokenValidator, redisKeyGenerator: redisKeyGenerator}
}

type redisTokenValidator struct {
	rdb               *redis.Client
	tokenHasher       SaltedHasher
	tokenValidator    TokenValidator
	redisKeyGenerator RedisKeyGenerator
}

func (v *redisTokenValidator) Validate(ctx context.Context, token string) (*TokenValOutput, error) {
	//check experitation time
	tokenValidator, err := v.tokenValidator.Validate(ctx, token)
	if err != nil {
		return nil, err
	}
	// check number of times token is valid
	hash, err := v.tokenHasher(token, tokenValidator.TokenSecret)
	if err != nil {
		return nil, err
	}
	key := v.redisKeyGenerator(tokenValidator.Username, hash)

	_, err = v.rdb.LPop(ctx, key).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			if err.Error() == "redis: nil" {
				return nil, &UnauthorizeError{errors.New("token used to many times")}
			}
		}
		return nil, err
	}

	return tokenValidator, nil
}
