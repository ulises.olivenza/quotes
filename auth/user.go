package auth

import (
	"encoding/hex"
	"errors"
)

type UserFinder interface {
	Find(username string) (*User, error)
}

func NewDummyUserFinder() (UserFinder, error) {
	salt, err := hex.DecodeString("3dbfdfa403c834f91d89269b7b0a3b2f3aed4958b8bf790774a8c6cc8eb5" +
		"cb21e4e84a40826623c317d197b11ab5a17fc36c48b9c6107c6c43903b9a76a9ac7804719f1a260d009c75")
	if err != nil {
		panic(err)
	}
	tokenSecret, err := hex.DecodeString("c807fc971f874892f87f86d4393d77d61f7cabc7ae9f306107c65759" +
		"a7bbad715fc2cd5df2df880c43903b9a76a9ac341942db28dfed7804719f1a260d009c75")
	if err != nil {
		panic(err)
	}
	hashser := NewPbkdf2Hasher()
	hashedPassword, err := hashser("password", salt)
	if err != nil {
		return nil, err
	}
	defaultUser := &User{"username", hashedPassword, salt, tokenSecret}
	return &dummyUserFinder{defaultUser: defaultUser}, nil
}

// Represents an user in the system
type User struct {
	Username       string
	HashedPassword string
	Salt           []byte
	TokenSecret    []byte
}

type dummyUserFinder struct {
	defaultUser *User
}

type UserPasswordNotFoundError struct {
	Err error
}

func (e *UserPasswordNotFoundError) Error() string {
	return e.Err.Error()
}

func (dus *dummyUserFinder) Find(username string) (*User, error) {
	if username == "username" {
		return dus.defaultUser, nil
	}
	return nil, &UserPasswordNotFoundError{errors.New("User/Password not found")}
}

type BasicAuthService interface {
	Auth(username string, password string) (*User, error)
}

func NewBasicAuthService(userFinder UserFinder, hashser SaltedHasher) BasicAuthService {
	return &basicAuthService{userFinder: userFinder, hashser: hashser}
}

type basicAuthService struct {
	userFinder UserFinder
	hashser    SaltedHasher
}

func (a *basicAuthService) Auth(username string, password string) (*User, error) {
	user, err := a.userFinder.Find(username)
	if err != nil {
		if _, ok := err.(*UserPasswordNotFoundError); ok {
			return nil, &UnauthorizeError{errors.New("User/Password is not authorized")}
		}
	}
	hashedPassword, err := a.hashser(password, user.Salt)
	if err != nil {
		return nil, err
	}
	if user.HashedPassword == hashedPassword {
		return user, nil
	}
	return nil, &UnauthorizeError{errors.New("User/Password is not authorized")}
}
