package auth_test

import (
	"os"
	"testing"
	"tgracchus/quotes/auth"
	"tgracchus/quotes/properties"
	"time"

	"github.com/go-redis/redis/v8"
)

var userFinder auth.UserFinder
var rdb *redis.Client
var tokenGenerator auth.TokenGenerator
var tokenValidator auth.TokenValidator
var hasher auth.SaltedHasher
var keyGenerator auth.RedisKeyGenerator
var redisTokenGenerator auth.TokenGenerator
var redisTokenValidator auth.TokenValidator
var basicAuthService auth.BasicAuthService

func TestMain(m *testing.M) {
	var err error
	userFinder, err = auth.NewDummyUserFinder()
	if err != nil {
		os.Exit(1)
	}
	redisHost := properties.GetEnv("REDIS_HOST", "localhost")
	redisPort := properties.GetEnv("REDIS_PORT", "6379")
	rdb = redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer rdb.Close()
	tokenGenerator = auth.NewJWTSecretTokenGenerator(userFinder)
	tokenValidator = auth.NewJWTSecretTokenValidator(userFinder)
	hasher = auth.NewPbkdf2Hasher()
	keyGenerator = auth.NewRedisKeyGenerator()
	redisTokenGenerator = auth.NewRedisTokenGenerator(
		rdb, hasher, tokenGenerator, keyGenerator,
	)

	redisTokenValidator = auth.NewRedisTokenValidator(
		rdb, hasher, tokenValidator, keyGenerator,
	)
	basicAuthService = auth.NewBasicAuthService(userFinder, hasher)
	os.Exit(m.Run())
}

func newTokenInfo() *auth.TokenGenInput {
	return &auth.TokenGenInput{
		Username:  "username",
		ExpiresAt: time.Now().Add(time.Minute),
		Times:     5,
	}
}
