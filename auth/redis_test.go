package auth_test

import (
	"context"
	"encoding/hex"
	"testing"
	"tgracchus/quotes/auth"
	"time"
)

func TestHashedToken(t *testing.T) {
	tokenInfo := newTokenInfo()
	user, err := userFinder.Find(tokenInfo.Username)
	if err != nil {
		t.Fatal(err)
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	token, err := tokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}

	if hex.EncodeToString(token.TokenSecret) != hex.EncodeToString(user.TokenSecret) {
		t.Fatalf("We expected token %x got %x", user.TokenSecret, token.TokenSecret)
	}
	_, err = hasher(token.Token, token.TokenSecret)
	if err != nil {
		t.Fatal(err)
	}
}

func TestRedisKeyGenerator(t *testing.T) {
	tokenInfo := newTokenInfo()
	tokenHash := "testhash"
	expectedKey := tokenInfo.Username + "/" + tokenHash
	key := keyGenerator(tokenInfo.Username, tokenHash)
	if key != expectedKey {
		t.Fatalf("Expected %s got %s", expectedKey, key)
	}
}

func TestRedisTokenGenerator(t *testing.T) {
	tokenInfo := newTokenInfo()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	token, err := redisTokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	hash, err := hasher(token.Token, token.TokenSecret)
	if err != nil {
		t.Fatal(err)
	}
	key := keyGenerator(tokenInfo.Username, hash)
	val, err := rdb.LPop(context.Background(), key).Int()
	if err != nil {
		t.Fatal(err)
	}
	if val != 1 {
		t.Fatal("Expecting 1")
	}
}

func TestRedisTokenGeneratorExpiredInRedis(t *testing.T) {
	tokenInfo := newTokenInfo()
	tokenInfo.ExpiresAt = time.Now().Add(time.Minute * -1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	generatedToken, err := redisTokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	hash, err := hasher(generatedToken.Token, generatedToken.TokenSecret)
	if err != nil {
		t.Fatal(err)
	}
	key := keyGenerator(tokenInfo.Username, hash)
	_, err = rdb.LPop(context.Background(), key).Int()
	if err == nil {
		t.Fatalf("An error was expected")
	}
}

func TestRedisTokenValidatorExpired(t *testing.T) {
	tokenInfo := newTokenInfo()
	tokenInfo.ExpiresAt = time.Now().Add(time.Minute * -1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	generatedToken, err := redisTokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	_, err = redisTokenValidator.Validate(ctx, generatedToken.Token)
	if err == nil {
		t.Fatalf("An error was expected")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}

func TestRedisTokenValidator(t *testing.T) {
	tokenInfo := newTokenInfo()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	generatedToken, err := redisTokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	_, err = redisTokenValidator.Validate(ctx, generatedToken.Token)
	if err != nil {
		t.Fatal(err)
	}
}

func TestRedisTokenExhaustToken(t *testing.T) {
	tokenInfo := newTokenInfo()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	generatedToken, err := redisTokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	for i := 0; i < tokenInfo.Times; i++ {
		_, err = redisTokenValidator.Validate(ctx, generatedToken.Token)
		if err != nil {
			t.Fatal(err)
		}
	}
	_, err = redisTokenValidator.Validate(ctx, generatedToken.Token)
	if err == nil {
		t.Fatal("Expected error")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}
