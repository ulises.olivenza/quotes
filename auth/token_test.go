package auth_test

import (
	"context"
	"testing"
	"tgracchus/quotes/auth"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func TestGenerateToken(t *testing.T) {
	tokenInfo := newTokenInfo()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err := tokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
}

func TestValidateToken(t *testing.T) {
	tokenInfo := newTokenInfo()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	token, err := tokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	tokenInfoValidated, err := tokenValidator.Validate(ctx, token.Token)
	if err != nil {
		t.Fatal(err)
	}
	if tokenInfo.Username != tokenInfoValidated.Username {
		t.Fatalf("Expected %s, get %s", tokenInfo.Username, tokenInfoValidated.Username)
	}
	if tokenInfo.ExpiresAt.Unix() != tokenInfoValidated.ExpiresAt.Unix() {
		t.Fatalf("Expected %v, get %v", tokenInfo.ExpiresAt, tokenInfoValidated.ExpiresAt)
	}
	if tokenInfo.Times != tokenInfoValidated.Times {
		t.Fatalf("Expected %d, get %d", tokenInfo.Times, tokenInfoValidated.Times)
	}
}

func TestExpiredToken(t *testing.T) {
	tokenInfo := newTokenInfo()
	tokenInfo.ExpiresAt = time.Now().Add(time.Minute * -1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	token, err := tokenGenerator.Generate(ctx, tokenInfo)
	if err != nil {
		t.Fatal(err)
	}
	_, err = tokenValidator.Validate(ctx, token.Token)
	if err == nil {
		t.Fatalf("An error was expected")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}

func TestParseTokenWithInvalidToken(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err := tokenValidator.Validate(ctx, "invalidToken")
	if err == nil {
		t.Fatalf("Expected an error")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
	if err.Error() != "token contains an invalid number of segments" {
		t.Fatalf("Error should oken contains an invalid number of segments get %s", err.Error())
	}
}

func TestParseTokenWithInvalidClaims(t *testing.T) {
	tokenInfo := newTokenInfo()
	secret := []byte("secret")
	claims := jwt.StandardClaims{
		ExpiresAt: tokenInfo.ExpiresAt.Unix(),
		Subject:   "username",
	}
	tokenWithInvalidClaims := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	invalidClaimsToken, err := tokenWithInvalidClaims.SignedString(secret)
	if err != nil {
		t.Fatal(err)
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = tokenValidator.Validate(ctx, invalidClaimsToken)
	if err == nil {
		t.Fatalf("Expected an error")
	}
	err, ok := err.(*auth.UnauthorizeError)
	if !ok {
		t.Fatal("Error should be an UnauthorizeError")
	}
}
