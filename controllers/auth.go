package controllers

import (
	"net/http"
	"tgracchus/quotes/auth"
	"tgracchus/quotes/middleware"
	"time"

	"github.com/labstack/echo/v4"
)

type AuthController struct {
	TokenGenerator auth.TokenGenerator
}

type TokenResult struct {
	Token string `json:"token"`
}

func (a *AuthController) Authentification(c echo.Context) error {
	username := c.Get(middleware.PrincipalKey).(string)
	tokenInfo := &auth.TokenGenInput{
		Username:  username,
		ExpiresAt: time.Now().Add(time.Minute * 1),
		Times:     5,
	}
	token, err := a.TokenGenerator.Generate(c.Request().Context(), tokenInfo)
	if err != nil {
		if err, ok := err.(*auth.UserPasswordNotFoundError); ok {
			return &echo.HTTPError{
				Code:     http.StatusBadRequest,
				Message:  err.Error(),
				Internal: err,
			}
		}
		return &echo.HTTPError{
			Code:     http.StatusInternalServerError,
			Message:  err.Error(),
			Internal: err,
		}
	}
	return c.JSON(http.StatusOK, &TokenResult{Token: token.Token})
}
