package controllers_test

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"tgracchus/quotes/controllers"
	"tgracchus/quotes/middleware"
	"tgracchus/quotes/quotes"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestFindAll(t *testing.T) {
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.FindAll(c)
		},
		"",
		http.StatusOK,
		nil,
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.FindAll(c)
		},
		"",
		http.StatusInternalServerError,
		errors.New("internalError"),
	)
}
func TestFind(t *testing.T) {
	quoteBytes, err := json.Marshal(&controllers.QuoteResult{Quote: quotesList[0].Quote})
	if err != nil {
		t.Fatal(err)
	}
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.Find(c)
		},
		addNewLine(quoteBytes),
		http.StatusOK,
		nil,
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.Find(c)
		},
		"",
		http.StatusNotFound,
		&quotes.QuoteNotFoundError{Err: errors.New("internalError")},
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.Find(c)
		},
		"",
		http.StatusInternalServerError,
		errors.New("internalError"),
	)
}

func TestShareURL(t *testing.T) {
	quoteBytes, err := json.Marshal(&controllers.SharedURL{SharedUrl: controllers.ShareBasePath + "/" + quotesList[0].Id})
	if err != nil {
		t.Fatal(err)
	}
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.ShareURL(c)
		},
		addNewLine(quoteBytes),
		http.StatusOK,
		nil,
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.ShareURL(c)
		},
		"",
		http.StatusNotFound,
		&quotes.QuoteNotFoundError{Err: errors.New("internalError")},
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.ShareURL(c)
		},
		"",
		http.StatusInternalServerError,
		errors.New("internalError"),
	)
}

func TestFindByShareURL(t *testing.T) {
	quoteBytes, err := json.Marshal(&controllers.QuoteResult{Quote: quotesList[0].Quote})
	if err != nil {
		t.Fatal(err)
	}
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.FindByShareURL(c)
		},
		addNewLine(quoteBytes),
		http.StatusOK,
		nil,
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.FindByShareURL(c)
		},
		"",
		http.StatusNotFound,
		&quotes.QuoteIDNotFoundError{Err: errors.New("internalError")},
	)
	testQuotes(t,
		func(c echo.Context, controller *controllers.QuotesController) error {
			return controller.FindByShareURL(c)
		},
		"",
		http.StatusInternalServerError,
		errors.New("internalError"),
	)
}

type mockFunc func(ctx echo.Context, controller *controllers.QuotesController) error

func testQuotes(t *testing.T, function mockFunc, expectedBody string, expectedStatus int, internalError error) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/quotes", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.Set(middleware.PrincipalKey, "username")

	mockServices := &mockServices{
		quotes: quotesList,
		quote:  quotesList[0],
	}

	quotesController := &controllers.QuotesController{
		Finder:        mockServices,
		QuoteIDGen:    mockServices,
		QuoteIDFinder: mockServices,
		BasePath:      controllers.ShareBasePath,
	}
	if internalError != nil {
		expectedError := &echo.HTTPError{
			Code:     expectedStatus,
			Message:  internalError.Error(),
			Internal: internalError,
		}
		mockServices.err = internalError
		err := function(c, quotesController)
		if assert.Error(t, err) {
			assert.Equal(t, expectedError, err)
		}

	} else {
		if assert.NoError(t, function(c, quotesController)) {
			assert.Equal(t, expectedStatus, rec.Code)
			if expectedBody != "" {
				assert.Equal(t, expectedBody, rec.Body.String())
			}

		}
	}

}

type mockServices struct {
	quote  *quotes.Quote
	quotes []*quotes.Quote
	err    error
}

func (m *mockServices) Find(ctx context.Context, id string, username string) (*quotes.Quote, error) {
	return m.quote, m.err
}
func (m *mockServices) FindAll(ctx context.Context, username string) ([]*quotes.Quote, error) {
	return m.quotes, m.err
}

func (m *mockServices) Generate(ctx context.Context, quoteId string, username string) (string, error) {
	return m.quote.Id, m.err
}

func (m *mockServices) FindQuoteByID(ctx context.Context, id string) (*quotes.Quote, error) {
	return m.quote, m.err
}
func addNewLine(bytes []byte) string {
	return string(bytes) + "\n"
}
