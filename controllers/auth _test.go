package controllers_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"tgracchus/quotes/auth"
	"tgracchus/quotes/controllers"
	"tgracchus/quotes/middleware"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestAuthIsOkReturnToken(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.Set(middleware.PrincipalKey, "username")

	tokenGenOutput := &auth.TokenGenOutput{
		Token:       "token",
		TokenSecret: []byte("secret"),
	}
	mockTokenGenerator := &mockTokenGenerator{
		tokenGenOutput: tokenGenOutput,
		err:            nil,
	}

	expectedBody := "{\"token\":\"token\"}\n"

	authController := &controllers.AuthController{TokenGenerator: mockTokenGenerator}
	if assert.NoError(t, authController.Authentification(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expectedBody, rec.Body.String())
	}
}

func TestAuthHasUserNotFoundErrorReturnBadRequest(t *testing.T) {
	testError(t, &auth.UserPasswordNotFoundError{Err: errors.New("not found error")}, http.StatusBadRequest)
}
func TestAuthHasErrorReturnInternalStatus(t *testing.T) {
	testError(t, errors.New("internal error"), http.StatusInternalServerError)
}

func testError(t *testing.T, returnedError error, expectedStatus int) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.Set(middleware.PrincipalKey, "username")

	mockTokenGenerator := &mockTokenGenerator{
		tokenGenOutput: nil,
		err:            returnedError,
	}

	expectedError := &echo.HTTPError{
		Code:     expectedStatus,
		Message:  returnedError.Error(),
		Internal: returnedError,
	}

	authController := &controllers.AuthController{TokenGenerator: mockTokenGenerator}
	err := authController.Authentification(c)
	if assert.Error(t, err) {
		assert.Equal(t, expectedError, err)
	}
}

type mockTokenGenerator struct {
	tokenGenOutput *auth.TokenGenOutput
	err            error
}

func (m *mockTokenGenerator) Generate(ctx context.Context, tokenGenInput *auth.TokenGenInput) (*auth.TokenGenOutput, error) {
	return m.tokenGenOutput, m.err
}
