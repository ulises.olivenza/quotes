package controllers

import (
	"net/http"
	"tgracchus/quotes/middleware"
	"tgracchus/quotes/quotes"

	"github.com/labstack/echo/v4"
)

const ShareBasePath = "/share"

type QuotesController struct {
	Finder        quotes.Finder
	QuoteIDGen    quotes.QuoteIDGenerator
	QuoteIDFinder quotes.QuoteIDFinder
	BasePath      string
}

// Represents a QuotesResult, we should introduce a new dto for quote object,
// but, since it's very simple and it's already anothated for json, I prefer to keep it like it is
type QuotesResults struct {
	User   string          `json:"user"`
	Quotes []*quotes.Quote `json:"quotes"`
}
type QuoteResult struct {
	Quote string `json:"quote"`
}

type SharedURL struct {
	SharedUrl string `json:"share_url"`
}

func (q *QuotesController) FindAll(c echo.Context) error {
	username := c.Get(middleware.PrincipalKey).(string)
	quotes, err := q.Finder.FindAll(c.Request().Context(), username)
	if err != nil {
		return &echo.HTTPError{
			Code:     http.StatusInternalServerError,
			Message:  err.Error(),
			Internal: err,
		}
	}
	return c.JSON(http.StatusOK, &QuotesResults{username, quotes})
}

func (q *QuotesController) Find(c echo.Context) error {
	quoteId := c.Param("quoteId")
	username := c.Get(middleware.PrincipalKey).(string)
	quote, err := q.Finder.Find(c.Request().Context(), quoteId, username)
	if err != nil {
		if err, ok := err.(*quotes.QuoteNotFoundError); ok {
			return &echo.HTTPError{
				Code:     http.StatusNotFound,
				Message:  err.Error(),
				Internal: err,
			}
		}
		return &echo.HTTPError{
			Code:     http.StatusInternalServerError,
			Message:  err.Error(),
			Internal: err,
		}
	}
	return c.JSON(http.StatusOK, &QuoteResult{quote.Quote})
}
func (q *QuotesController) ShareURL(c echo.Context) error {
	quoteId := c.Param("quoteId")
	username := c.Get(middleware.PrincipalKey).(string)
	id, err := q.QuoteIDGen.Generate(c.Request().Context(), quoteId, username)
	if err != nil {
		if err, ok := err.(*quotes.QuoteNotFoundError); ok {
			return &echo.HTTPError{
				Code:     http.StatusNotFound,
				Message:  err.Error(),
				Internal: err,
			}
		}
		return &echo.HTTPError{
			Code:     http.StatusInternalServerError,
			Message:  err.Error(),
			Internal: err,
		}
	}
	return c.JSON(http.StatusOK, &SharedURL{q.BasePath + "/" + id})
}
func (q *QuotesController) FindByShareURL(c echo.Context) error {
	sharedUrl := c.Param("shareUrl")
	quote, err := q.QuoteIDFinder.FindQuoteByID(c.Request().Context(), sharedUrl)
	if err != nil {
		if err, ok := err.(*quotes.QuoteIDNotFoundError); ok {
			return &echo.HTTPError{
				Code:     http.StatusNotFound,
				Message:  err.Error(),
				Internal: err,
			}
		}
		return &echo.HTTPError{
			Code:     http.StatusInternalServerError,
			Message:  err.Error(),
			Internal: err,
		}
	}
	return c.JSON(http.StatusOK, &QuoteResult{Quote: quote.Quote})
}
