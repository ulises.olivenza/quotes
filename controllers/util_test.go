package controllers_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"tgracchus/quotes/properties"
	"tgracchus/quotes/quotes"
)

var quotesList []*quotes.Quote

func TestMain(m *testing.M) {
	quotesFile := properties.GetEnv("QUOTES_FILE", "../quotes.json")
	byteValue, err := ioutil.ReadFile(quotesFile)
	if err != nil {
		os.Exit(1)
	}
	err = json.Unmarshal(byteValue, &quotesList)
	if err != nil {
		os.Exit(1)
	}
	os.Exit(m.Run())
}
