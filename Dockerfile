FROM golang:1.16.4-alpine3.13 as builder
WORKDIR /go/src/quotes
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o main cmd/main.go

FROM alpine:3.13 as base
COPY quotes.json quotes.json
COPY --from=builder /go/src/quotes/main main
ENTRYPOINT ["./main"]
