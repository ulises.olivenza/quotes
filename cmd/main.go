package main

import (
	"tgracchus/quotes/auth"
	"tgracchus/quotes/controllers"
	"tgracchus/quotes/middleware"
	"tgracchus/quotes/properties"
	"tgracchus/quotes/quotes"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	emiddleware "github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

func main() {
	e := echo.New()
	e.Use(emiddleware.Logger())
	e.Use(emiddleware.Recover())
	e.Logger.SetLevel(log.DEBUG)

	redisHost := properties.GetEnv("REDIS_HOST", "localhost")
	redisPort := properties.GetEnv("REDIS_PORT", "6379")
	quotesFile := properties.GetEnv("QUOTES_FILE", "../quotes.json")
	// Depencency Injection
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	userFinder, err := auth.NewDummyUserFinder()
	if err != nil {
		e.Logger.Fatal(err)
	}
	tokenGenerator := auth.NewJWTSecretTokenGenerator(userFinder)
	tokenValidator := auth.NewJWTSecretTokenValidator(userFinder)
	hasher := auth.NewPbkdf2Hasher()
	userAuth := auth.NewBasicAuthService(userFinder, hasher)

	keyGenerator := auth.NewRedisKeyGenerator()
	redisTokenGenerator := auth.NewRedisTokenGenerator(
		rdb, hasher, tokenGenerator, keyGenerator,
	)
	redisTokenValidator := auth.NewRedisTokenValidator(
		rdb, hasher, tokenValidator, keyGenerator,
	)

	// AuthController
	authGroup := e.Group("/auth")
	authGroup.Use(middleware.NewBasicAuth(userAuth))
	authController := &controllers.AuthController{TokenGenerator: redisTokenGenerator}
	authGroup.POST("", authController.Authentification)

	// QuotesControler
	finder, err := quotes.NewRedisQuoteFinder(rdb, "quotes", quotesFile)
	if err != nil {
		e.Logger.Fatal(err)
	}
	urlGen := quotes.NewIDGenerator(128)
	sharedUrlGen := quotes.NewQuoteIDGenerator(rdb, finder, urlGen)
	shatedUrlFinder := quotes.NewQuoteIDFinder(rdb)
	quotesController := &controllers.QuotesController{Finder: finder, QuoteIDGen: sharedUrlGen, QuoteIDFinder: shatedUrlFinder, BasePath: controllers.ShareBasePath}

	quotesGroup := e.Group("/quotes")
	quotesGroup.Use(middleware.JWT(redisTokenValidator))
	quotesGroup.GET("", quotesController.FindAll)
	quotesGroup.GET("/:quoteId", quotesController.Find)
	quotesGroup.GET("/:quoteId/share", quotesController.ShareURL)

	// SharedController
	shareGroup := e.Group(controllers.ShareBasePath)
	shareGroup.GET("/:shareUrl", quotesController.FindByShareURL)

	e.Logger.Fatal(e.Start(":8080"))
}
