#!/usr/bin/env bash
set -e

export REDIS_HOST="redis"
cd /go/src/quotes
go test ./auth 
go test ./quotes 
go test ./middleware 
go test ./controllers 