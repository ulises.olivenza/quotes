#!/usr/bin/env bash
set -e

docker run --network=quotes_default -v"$(pwd)":/go/src/quotes/ golang:1.16.4 "/go/src/quotes/scripts/docker-test.sh"
