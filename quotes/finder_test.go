package quotes_test

import (
	"context"
	"testing"
	"tgracchus/quotes/quotes"
)

func TestQuotes(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	quoteFinder, err := quotes.NewRedisQuoteFinder(rdb, "quotes", "../quotes.json")
	if err != nil {
		t.Fatal(err)
	}
	quote, err := quoteFinder.Find(ctx, "16166cf3", "username")
	if err != nil {
		t.Fatal(err)
	}
	if quote.Quote != "The secret of getting ahead is getting started." {
		t.Fatal(err)
	}
}

func TestFindAllQuotes(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	quoteFinder, err := quotes.NewRedisQuoteFinder(rdb, "quotes", "../quotes.json")
	if err != nil {
		t.Fatal(err)
	}
	quotes, err := quoteFinder.FindAll(ctx, "username")
	if err != nil {
		t.Fatal(err)
	}
	if len(quotes) != 31 {
		t.Fatal("Expected 31 quotes")
	}
}

func TestQuoteNotFound(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	quoteFinder, err := quotes.NewRedisQuoteFinder(rdb, "quotes", "../quotes.json")
	if err != nil {
		t.Fatal(err)
	}
	_, err = quoteFinder.Find(ctx, "notFound", "username")
	if err == nil {
		t.Fatal("Expected error")
	}
	err, ok := err.(*quotes.QuoteNotFoundError)
	if !ok {
		t.Fatal("Error should be an ErrQuoteNotFound")
	}
}

func TestQuoteNotFoundUserNotPresent(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	quoteFinder, err := quotes.NewRedisQuoteFinder(rdb, "quotes", "../quotes.json")
	if err != nil {
		t.Fatal(err)
	}
	_, err = quoteFinder.Find(ctx, "16166cf3", "nouser")
	if err == nil {
		t.Fatal("Expected error")
	}
	err, ok := err.(*quotes.QuoteNotFoundError)
	if !ok {
		t.Fatal("Error should be an ErrQuoteNotFound")
	}
}
