package quotes_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"tgracchus/quotes/properties"
	"tgracchus/quotes/quotes"

	"github.com/go-redis/redis/v8"
)

var rdb *redis.Client
var quotesList []*quotes.Quote
var quoteFinder quotes.Finder
var idGenerator quotes.IDGenerator
var quoteIdGenerator quotes.QuoteIDGenerator
var quoteIdFinder quotes.QuoteIDFinder

func TestMain(m *testing.M) {
	redisHost := properties.GetEnv("REDIS_HOST", "localhost")
	redisPort := properties.GetEnv("REDIS_PORT", "6379")
	rdb = redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer rdb.Close()

	quotesFile := properties.GetEnv("QUOTES_FILE", "../quotes.json")
	byteValue, err := ioutil.ReadFile(quotesFile)
	if err != nil {
		os.Exit(1)
	}
	err = json.Unmarshal(byteValue, &quotesList)
	if err != nil {
		os.Exit(1)
	}
	idGenerator = quotes.NewIDGenerator(256)
	quoteFinder, err = quotes.NewRedisQuoteFinder(rdb, "quotes", quotesFile)
	if err != nil {
		os.Exit(1)
	}
	quoteIdFinder = quotes.NewQuoteIDFinder(rdb)
	quoteIdGenerator = quotes.NewQuoteIDGenerator(rdb, quoteFinder, idGenerator)

	os.Exit(m.Run())
}
