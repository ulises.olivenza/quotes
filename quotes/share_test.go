package quotes_test

import (
	"context"
	"reflect"
	"testing"
	"tgracchus/quotes/quotes"
)

func TestQuoteURLGenerator(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	quote := quotesList[0]
	url, err := quoteIdGenerator.Generate(ctx, quote.Id, "username")
	if err != nil {
		t.Fatal(err)
	}

	foundQuote, err := quoteIdFinder.FindQuoteByID(ctx, url)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(quote, foundQuote) {
		t.Fatalf("Expected %s but got %s", quote, foundQuote)
	}
}

func TestQuoteURLGeneratorOnlyOneUse(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	quote := quotesList[0]
	url, err := quoteIdGenerator.Generate(ctx, quote.Id, "username")
	if err != nil {
		t.Fatal(err)
	}
	foundQuote, err := quoteIdFinder.FindQuoteByID(ctx, url)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(quote, foundQuote) {
		t.Fatalf("Expected %v but got %v", quote, foundQuote)
	}
	_, err = quoteIdFinder.FindQuoteByID(ctx, url)
	if err == nil {
		t.Fatal("Expected error")
	}
	err, ok := err.(*quotes.QuoteIDNotFoundError)
	if !ok {
		t.Fatal("Error should be an SharedURLNotFoundError")
	}
}

func TestQuoteURLGeneratorNotFound(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err := quoteIdFinder.FindQuoteByID(ctx, "notFound")
	if err == nil {
		t.Fatal("Expected error")
	}
	err, ok := err.(*quotes.QuoteIDNotFoundError)
	if !ok {
		t.Fatal("Error should be an SharedURLNotFoundError")
	}
}
