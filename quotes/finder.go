package quotes

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/go-redis/redis/v8"
)

type Quote struct {
	Id    string `json:"id"`
	Quote string `json:"quote"`
}

// We are using plain json, but, in real we will probabily use another encoders, specially binary, like avro or protocol buffers
func Encode(q *Quote) (string, error) {
	bytes, err := json.Marshal(q)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func Decode(q string) (*Quote, error) {
	var quote Quote
	err := json.Unmarshal([]byte(q), &quote)
	if err != nil {
		return nil, err
	}
	return &quote, nil
}

type Finder interface {
	Find(ctx context.Context, id string, username string) (*Quote, error)
	FindAll(ctx context.Context, username string) ([]*Quote, error)
}

type QuoteNotFoundError struct {
	Err error
}

func (e QuoteNotFoundError) Error() string {
	return e.Err.Error()
}

type quoteFinder struct {
	quotesKey string
	rdb       *redis.Client
}

func (f *quoteFinder) FindAll(ctx context.Context, username string) ([]*Quote, error) {
	quotesMap, err := f.rdb.HGetAll(ctx, f.quotesKey+"/"+username).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			if err.Error() == "redis: nil" {
				return nil, &QuoteNotFoundError{errors.New("quotes not found")}
			}
		}
		return nil, err
	}
	quotes := make([]*Quote, 0, len(quotesMap))
	for _, value := range quotesMap {
		quote, err := Decode(value)
		if err != nil {
			return nil, err
		}
		quotes = append(quotes, quote)
	}
	return quotes, nil
}

func (f *quoteFinder) Find(ctx context.Context, id string, username string) (*Quote, error) {
	quoteJson, err := f.rdb.HGet(ctx, f.quotesKey+"/"+username, id).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			if err.Error() == "redis: nil" {
				return nil, &QuoteNotFoundError{fmt.Errorf("Quote %s not found", id)}
			}
		}
		return nil, err
	}
	return Decode(quoteJson)
}

func NewRedisQuoteFinder(rdb *redis.Client, quotesKey string, file string) (Finder, error) {
	byteValue, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	var quotes []*Quote
	err = json.Unmarshal(byteValue, &quotes)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var encodedQuotes map[string]interface{} = make(map[string]interface{})
	for i := 0; i < len(quotes); i++ {
		quote := quotes[i]
		quoteJson, err := Encode(quote)
		if err != nil {
			return nil, err
		}
		encodedQuotes[quote.Id] = quoteJson
	}

	_, err = rdb.HSet(ctx, quotesKey+"/username", encodedQuotes).Result()
	if err != nil {
		return nil, err
	}

	return &quoteFinder{rdb: rdb, quotesKey: quotesKey}, nil
}
