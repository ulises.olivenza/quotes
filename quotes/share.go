package quotes

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/go-redis/redis/v8"
)

type IDGenerator interface {
	Generate(ctx context.Context) (string, error)
}

type QuoteIDGenerator interface {
	Generate(ctx context.Context, quoteId string, username string) (string, error)
}

type QuoteIDFinder interface {
	FindQuoteByID(ctx context.Context, id string) (*Quote, error)
}

type QuoteIDNotFoundError struct {
	Err error
}

func NewIDGenerator(size int) IDGenerator {
	return &secureUrlGenerator{size: size}
}

type secureUrlGenerator struct {
	size int
}

func (u *secureUrlGenerator) Generate(ctx context.Context) (string, error) {
	var b = make([]byte, u.size)
	_, err := rand.Read(b[:])
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

func NewQuoteIDGenerator(rdb *redis.Client, finder Finder, urlGenerator IDGenerator) QuoteIDGenerator {
	return &redisUrlGenerator{rdb: rdb, urlGenerator: urlGenerator, finder: finder}
}

type redisUrlGenerator struct {
	rdb          *redis.Client
	urlGenerator IDGenerator
	finder       Finder
}

func (r *redisUrlGenerator) Generate(ctx context.Context, quoteId string, username string) (string, error) {
	quote, err := r.finder.Find(ctx, quoteId, username)
	if err != nil {
		return "", err
	}
	url, err := r.urlGenerator.Generate(ctx)
	if err != nil {
		return "", err
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	q, err := Encode(quote)
	if err != nil {
		return "", err
	}
	_, err = r.rdb.LPush(ctx, url, q).Result()
	if err != nil {
		return "", err
	}
	return url, nil
}

func (e *QuoteIDNotFoundError) Error() string {
	return e.Err.Error()
}

func NewQuoteIDFinder(rdb *redis.Client) QuoteIDFinder {
	return &redisUrlFinder{rdb: rdb}
}

type redisUrlFinder struct {
	rdb *redis.Client
}

func (f *redisUrlFinder) FindQuoteByID(ctx context.Context, id string) (*Quote, error) {
	quoteJson, err := f.rdb.LPop(ctx, id).Result()
	if err != nil {
		if err, ok := err.(redis.Error); ok {
			if err.Error() == "redis: nil" {
				return nil, &QuoteIDNotFoundError{fmt.Errorf("id not found %s", id)}
			}
		}
		return nil, err
	}
	return Decode(quoteJson)
}
