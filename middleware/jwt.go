package middleware

import (
	"errors"
	"net/http"
	"tgracchus/quotes/auth"

	"github.com/labstack/echo/v4"
)

// JWT returns a JWT auth middleware
func JWT(tokenValidator auth.TokenValidator) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			token, err := jwtFromHeader(c)
			if err != nil {
				return &echo.HTTPError{
					Code:     http.StatusUnauthorized,
					Message:  err.Error(),
					Internal: err,
				}
			}
			validation, err := tokenValidator.Validate(c.Request().Context(), token)
			if err != nil {
				if err, ok := err.(*auth.UnauthorizeError); ok {
					return &echo.HTTPError{
						Code:     http.StatusUnauthorized,
						Message:  err.Error(),
						Internal: err,
					}
				}
				return &echo.HTTPError{
					Code:     http.StatusInternalServerError,
					Message:  err.Error(),
					Internal: err,
				}
			}

			c.Set(PrincipalKey, validation.Username)
			return next(c)
		}

	}
}

// jwtFromHeader returns a `jwtExtractor` that extracts token from the request header.
func jwtFromHeader(c echo.Context) (string, error) {
	authScheme := "Bearer"
	authHeader := c.Request().Header.Get(echo.HeaderAuthorization)
	l := len(authScheme)
	if len(authHeader) > l+1 && authHeader[:l] == authScheme {
		return authHeader[l+1:], nil
	}
	return "", &auth.UnauthorizeError{Err: errors.New("invalid token")}
}
