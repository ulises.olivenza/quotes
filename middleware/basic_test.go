package middleware_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"tgracchus/quotes/auth"
	"tgracchus/quotes/middleware"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

var handler = func(c echo.Context) error {
	return c.String(http.StatusOK, "test")
}

func TestBasicIsOk(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	handler := func(c echo.Context) error {
		return c.String(http.StatusOK, "test")
	}
	user := &auth.User{Username: "username", HashedPassword: "asdasd", Salt: []byte("salt"), TokenSecret: []byte("tokenSecret")}
	authController := middleware.NewBasicAuth(&mockAuthService{user: user})(handler)
	if assert.NoError(t, authController(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, "test", rec.Body.String())
	}
}

func TestBasicMiddlewareUnauthorizeError(t *testing.T) {
	testBasicMiddlewareError(t, errors.New("error"), http.StatusInternalServerError)
}

func TestBasicMiddlewareInternalError(t *testing.T) {
	testBasicMiddlewareError(t, &auth.UnauthorizeError{Err: errors.New("error")}, http.StatusBadRequest)
}

func testBasicMiddlewareError(t *testing.T, internalError error, statusCode int) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	expectedError := &echo.HTTPError{
		Code:     statusCode,
		Message:  internalError.Error(),
		Internal: internalError,
	}
	assert := assert.New(t)
	authController := middleware.NewBasicAuth(&mockAuthService{err: internalError})(handler)
	err := authController(c)
	assert.Error(err)
	assert.Equal(expectedError, err)
}

type mockAuthService struct {
	user *auth.User
	err  error
}

func (s *mockAuthService) Auth(username string, password string) (*auth.User, error) {
	return s.user, s.err
}
