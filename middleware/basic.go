package middleware

import (
	"net/http"
	"tgracchus/quotes/auth"

	"github.com/labstack/echo/v4"
	emiddleware "github.com/labstack/echo/v4/middleware"
)

const PrincipalKey = "principal"

func NewBasicAuth(userAuth auth.BasicAuthService) echo.MiddlewareFunc {
	return emiddleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		user, err := userAuth.Auth(username, password)
		if err != nil {
			if err, ok := err.(*auth.UnauthorizeError); ok {
				return false, &echo.HTTPError{
					Code:     http.StatusBadRequest,
					Message:  err.Error(),
					Internal: err,
				}
			}
			return false, &echo.HTTPError{
				Code:     http.StatusInternalServerError,
				Message:  err.Error(),
				Internal: err,
			}
		}
		c.Set(PrincipalKey, user.Username)
		return true, nil
	})
}
