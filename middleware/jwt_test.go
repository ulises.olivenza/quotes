package middleware_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"tgracchus/quotes/auth"
	"tgracchus/quotes/middleware"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestJWTMiddlewareIsOk(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, "Bearer dXNlcm5hbWU6cGFzc3dvcmQ=")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	handler := func(c echo.Context) error {
		return c.String(http.StatusOK, "test")
	}
	tokenValOutput := &auth.TokenValOutput{Username: "username", Times: 5, ExpiresAt: time.Now(), TokenSecret: []byte("tokenSecret")}
	authController := middleware.JWT(&mockTokenValidator{tokenValOutput: tokenValOutput})(handler)
	if assert.NoError(t, authController(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, "test", rec.Body.String())
	}
}
func TestJWTMiddlewareInternalError(t *testing.T) {
	testJWTMiddlewareError(t, "Bearer dXNlcm5hbWU6cGFzc3dvcmQ=", errors.New("error"), http.StatusInternalServerError)
}

func TestJWTMiddlewarenUnauthorizeError(t *testing.T) {
	testJWTMiddlewareError(t, "Bearer dXNlcm5hbWU6cGFzc3dvcmQ=", &auth.UnauthorizeError{Err: errors.New("error")}, http.StatusUnauthorized)
}

func TestJWTMiddlewarenWrongHeader(t *testing.T) {
	testJWTMiddlewareError(t, "Authorization dXNlcm5hbWU6cGFzc3dvcmQ=", &auth.UnauthorizeError{Err: errors.New("invalid token")}, http.StatusUnauthorized)
}

func testJWTMiddlewareError(t *testing.T, token string, internalError error, statusCode int) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/auth", nil)
	req.Header.Set(echo.HeaderAuthorization, token)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	expectedError := &echo.HTTPError{
		Code:     statusCode,
		Message:  internalError.Error(),
		Internal: internalError,
	}
	assert := assert.New(t)
	authController := middleware.JWT(&mockTokenValidator{err: internalError})(handler)
	err := authController(c)
	assert.Error(err)
	assert.Equal(expectedError, err)
}

type mockTokenValidator struct {
	tokenValOutput *auth.TokenValOutput
	err            error
}

func (s *mockTokenValidator) Validate(ctx context.Context, token string) (*auth.TokenValOutput, error) {
	return s.tokenValOutput, s.err
}
